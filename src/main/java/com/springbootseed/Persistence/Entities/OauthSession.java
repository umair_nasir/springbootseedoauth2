package com.springbootseed.Persistence.Entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.util.Date;

/**
 * Created by umair.nasir on 4/10/17.
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OauthSession {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    String id;
    String accessToken;
    Date oauthSessionStart;
    Date oauthSessionEnd;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    Login login;
}
