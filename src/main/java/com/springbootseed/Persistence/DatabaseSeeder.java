package com.springbootseed.Persistence;


import com.springbootseed.Constants;
import com.springbootseed.Persistence.Entities.Login;
import com.springbootseed.Persistence.Entities.User;
import com.springbootseed.Persistence.Repositories.LoginRepo;
import com.springbootseed.Persistence.Repositories.UserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Date;

/**
 * Created by umair.nasir on 12/6/16.
 */

@Service
public class DatabaseSeeder {

    @Autowired
    UserRepo userRepo;

    @Autowired
    LoginRepo loginRepo;

    @Autowired
    PasswordEncoder passwordEncoder;

    @PostConstruct
    private void createUser() {
        User user = new User();
        user.setUserName("Umair Nasir");
        user.setPermissions((Constants.Permissions.ADMIN));

        Login login = new Login();
        login.setUser(user);
        login.setUserName(user.getUserName());
        login.setEmail("nasirumair@hotmail.com");
        if (!userRepo.findAll().isEmpty() | !loginRepo.findAll().isEmpty()){
            return;
        }
        userRepo.saveAndFlush(user);
        loginRepo.saveAndFlush(login);
    }

}
