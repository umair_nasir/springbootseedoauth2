package com.springbootseed.Persistence.Repositories;

import com.springbootseed.Persistence.Entities.Login;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by umair.nasir on 3/23/17.
 */

@Repository
public interface LoginRepo extends JpaRepository<Login, String> {
    Login findByUserName(String name);
    Login findByUserNameAndEmail(String name, String id);
}