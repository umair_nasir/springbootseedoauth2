package com.springbootseed.Persistence.Repositories;

import com.springbootseed.Persistence.Entities.OauthSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by umair.nasir on 4/10/17.
 */
@Repository
public interface OauthSessionRepo extends JpaRepository<OauthSession, String> {
}
