package com.springbootseed.Persistence.Repositories;

import com.springbootseed.Persistence.Entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by umair.nasir on 3/23/17.
 */

@Repository
public interface UserRepo extends JpaRepository<User, String> {
    User findByUserName(String name);
}