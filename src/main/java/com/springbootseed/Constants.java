package com.springbootseed;

/**
 * Created by umair.nasir on 3/23/17.
 */
public class Constants {

    public class Permissions {
        public static final int ADMIN = 1 << 0;
        public static final int USER = 1 << 1;
    }

    public class Session {
        public static final String SESSION_ID = "sess";
        public static final String LOGIN_CLAIM = "lclaim";
        public static final String PERMISSIONS = "permissions";
    }

    public class Headers {
        public static final String HEADER_TOKEN = "Authorization";
        public static final String HEADER_REQUEST_CLIENT = "X-request-client";
        public static final String HEADER_CONTENT_TYPE = "Content-Type";
        public static final String HEADER_ALLOW_HEADERS = "Access-Control-Allow-Headers";
    }

}