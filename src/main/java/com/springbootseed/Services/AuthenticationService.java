package com.springbootseed.Services;

import com.springbootseed.Constants;
import com.springbootseed.Persistence.Entities.Login;
import com.springbootseed.Persistence.Entities.OauthSession;
import com.springbootseed.Persistence.Entities.User;
import com.springbootseed.Persistence.Repositories.LoginRepo;
import com.springbootseed.Persistence.Repositories.OauthSessionRepo;
import com.springbootseed.Services.Security.LoginContext;
import io.jsonwebtoken.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.social.RejectedAuthorizationException;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by umair.nasir on 3/22/17.
 */
@Service
public class AuthenticationService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${session.expiry_seconds}")
    private int SESSION_EXPIRY_SECONDS;
    @Value("${token.expiry_seconds}")
    private int EXPIRATION_TIME_IN_SECONDS;
    @Value("${token.secret_key}")
    private String SECRET;

    @Autowired
    private LoginRepo loginRepo;

    @Autowired
    OauthSessionRepo oauthSessionRepo;

    public Login getFacebookMappedUser(String accessToken) {
        Facebook facebook = new FacebookTemplate(accessToken);
        try {
            org.springframework.social.facebook.api.User facebookUser = facebook.fetchObject("me", org.springframework.social.facebook.api.User.class, "email, name, id");
            Login login = loginRepo.findByUserNameAndEmail(facebookUser.getName(), facebookUser.getEmail());
            return login;
        } catch (RejectedAuthorizationException e) {
            log.info("Invalid Access Token");
            throw new AccessDeniedException("Invalid Access Token");
        }
    }

    public String createToken(Login login, String sessionId) {
        User user = login.getUser();

        LoginClaim loginClaim = null;
        if (user != null) {
            loginClaim = new LoginClaim(login.getUser().getId(), login.getUser().getPermissions());
        }

        String JWT = Jwts.builder()
                .setSubject(login.getUserName())
                .setExpiration(DateTime.now().plusSeconds(EXPIRATION_TIME_IN_SECONDS).toDate())
                .claim(Constants.Session.SESSION_ID, sessionId)
                .claim(Constants.Session.LOGIN_CLAIM, loginClaim)
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();

        return JWT;
    }

    public String createNewOauth(String accessToken, int expiresIn, Login login) {
        OauthSession oauthSession = new OauthSession();
        oauthSession.setAccessToken(accessToken);
        oauthSession.setOauthSessionStart(new Date());
        oauthSession.setOauthSessionEnd(DateTime.now().plusSeconds(expiresIn).toDate());
        oauthSession.setLogin(login);
        return oauthSessionRepo.saveAndFlush(oauthSession).getId();
    }

    public Login checkSession(String sessionId) {
        OauthSession session = oauthSessionRepo.findOne(sessionId);

        if (session == null) {
            log.info("No session found with that token: Fail");
            return null;
        }
        Login login = session.getLogin();

        log.debug("Found login: " + login.getUserName());

        if (!checkSession_isValid(session.getOauthSessionEnd())) {
            return null;
        }
        return login;
    }

    public boolean endOauthSession(String oAuthSessionId) {
        OauthSession oauthSession = oauthSessionRepo.findOne(oAuthSessionId);
        if (oauthSession == null) {
            log.warn("During logout, session ID not found: " + oauthSession);
            return false;
        }
        Login login = oauthSession.getLogin();

        if (checkSession_isValid(oauthSession.getOauthSessionEnd())) {
            Facebook facebook = new FacebookTemplate(oauthSession.getAccessToken());
            facebook.restOperations().delete(facebook.getBaseGraphApiUrl() + "/me/permissions?access_token=" + oauthSession.getAccessToken());
            log.info("Logged out of Facebook OAuth");

            oauthSession.setOauthSessionEnd(new Date());
            oauthSessionRepo.saveAndFlush(oauthSession);
            log.info("Successfully logged out login: " + login.getUserName());
            return true;
        }
        return false;
    }

    private Object getCurrentPrincipal() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication == null ? null : authentication.getPrincipal();
    }

    public boolean logout(String token) {
        Claims claims;
        try {
            claims = parse(token);
        } catch (ExpiredJwtException e) {
            claims = e.getClaims();
        } catch (SignatureException e) {
            log.warn("Invalid token signature!");
            return false;
        } catch (MalformedJwtException ex) {
            log.warn("Badly formed token!");
            return false;
        }
        String sessionId = claims.get(Constants.Session.SESSION_ID, String.class);
        if (endOauthSession(sessionId)) {
            SecurityContextHolder.clearContext();
        }
        return true;
    }

    private Claims parse(String token) {
        return Jwts.parser()
                .setSigningKey(SECRET)
                .parseClaimsJws(token)
                .getBody();
    }

    public boolean checkSession_isValid(Date sessionEndDate) {
        if (new Date().after(sessionEndDate)) {
            log.info("Session found, but expired.");
            return false;
        }
        log.info("OAuth session is still valid");
        return true;
    }

    public String checkToken(String token) {
        log.info("Checking token: " + token);

        PreAuthenticatedAuthenticationToken securityToken;
        try {
            Claims body = parse(token);
            HashMap loginClaim = body.get(Constants.Session.LOGIN_CLAIM, HashMap.class);
            Collection<GrantedAuthority> authorities = new ArrayList<>();

            if (loginClaim != null) {
                int permissions = (int) loginClaim.get(Constants.Session.PERMISSIONS);
                if ((permissions & Constants.Permissions.ADMIN) > 0)
                    authorities.addAll(LoginContext.getAdminAuthorities());

                if ((permissions & Constants.Permissions.USER) > 0) {
                    authorities.addAll(LoginContext.getUserAuthorities());
                }
            }
            log.info("Token accepted");
            log.debug("Granting authorities: " + authorities);
            securityToken = new PreAuthenticatedAuthenticationToken(token, null, authorities);
        } catch (ExpiredJwtException ex) {
            log.info("Token expired");
            Claims claims = ex.getClaims();

            String sessionId = (String) claims.get(Constants.Session.SESSION_ID);
            Login login = checkSession(sessionId);
            if (login == null) return null;

            UserDetails userDetails = new LoginContext(login);
            log.debug("Granting authorities: " + userDetails.getAuthorities());
            token = createToken(login, sessionId);
            log.info("Issuing new token: " + token);

            securityToken = new PreAuthenticatedAuthenticationToken(
                    userDetails, null, userDetails.getAuthorities());
        } catch (SignatureException e) {
            log.warn("Invalid token signature!");
            return null;
        } catch (MalformedJwtException ex) {
            log.warn("Badly formed token!");
            return null;
        }
        SecurityContextHolder.getContext().setAuthentication(securityToken);
        return token;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    private class LoginClaim {
        String id;
        int permissions;
    }
}
