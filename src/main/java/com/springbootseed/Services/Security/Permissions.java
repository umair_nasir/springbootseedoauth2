package com.springbootseed.Services.Security;

/**
 * Created by umair.nasir on 3/23/17.
 */

public class Permissions {
    public static final String ADMINISTER_ORGANIZATION = "ROLE_ADMINISTER_ORGANIZATION";
    public static final String USER_ORGANIZATION = "ROLE_USER_ORGANIZATION";
}
