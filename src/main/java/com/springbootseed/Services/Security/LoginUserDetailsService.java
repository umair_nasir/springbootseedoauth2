package com.springbootseed.Services.Security;

import com.springbootseed.Persistence.Entities.Login;
import com.springbootseed.Persistence.Repositories.LoginRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * Created by umair.nasir on 3/23/17.
 */

@Component("userDetailsService")
@Service
public class LoginUserDetailsService implements UserDetailsService {

    @Autowired
    LoginRepo loginRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Login login = loginRepo.findByUserName(username);
        if(login == null){
            throw new UsernameNotFoundException("Login "+ username + "not found");
        }

        return new LoginContext(login);
    }
}
