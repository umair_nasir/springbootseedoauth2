package com.springbootseed.Services.Security;

import com.springbootseed.Constants;
import com.springbootseed.Persistence.Entities.Login;
import com.springbootseed.Persistence.Entities.User;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

import static java.util.Arrays.asList;

/**
 * Created by umair.nasir on 3/23/17.
 */

@Getter
public class LoginContext implements UserDetails {

    private Login login;
    private String userId;

    public LoginContext(Login login){
        this.login = login;
        this.userId = login.getId();
    }

    public static Collection<GrantedAuthority> getAdminAuthorities() {
        return asList(
                new SimpleGrantedAuthority(Permissions.ADMINISTER_ORGANIZATION)
        );
    }

    public static Collection<GrantedAuthority> getUserAuthorities() {
        return asList(
                new SimpleGrantedAuthority (Permissions.USER_ORGANIZATION)
        );
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        Collection<GrantedAuthority> authorities = new ArrayList<>();
        User user = login.getUser();

        if ((user.getPermissions() & Constants.Permissions.ADMIN) > 0)
            authorities.addAll(getAdminAuthorities());

        if ((user.getPermissions() & Constants.Permissions.USER) > 0)
            authorities.addAll(getUserAuthorities());

        return authorities;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return login.getUserName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String toString() {
        return "User context: " + userId;
    }
}
