package com.springbootseed.Services.Security;

import com.springbootseed.Services.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.springbootseed.Constants.Headers.HEADER_TOKEN;

/**
 * Created by umair.nasir on 3/29/17.
 */

@Service
public class AuthenticationFilter extends GenericFilterBean {

    private static final String LOGOUT = "/logout/facebook";
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    AuthenticationService authenticationService;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        checkToken(httpRequest, httpResponse);
        if (currentLink(httpRequest).equals(LOGOUT)) {
            if (doLogout(httpRequest, httpResponse))
                return;
        }
        filterChain.doFilter(request, response);
    }

    private String checkToken(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws IOException {
        String token = httpRequest.getHeader(HEADER_TOKEN);

        if (token == null) {
            return null;
        }

        String newToken = authenticationService.checkToken(token);

        if (newToken != null) {
            httpResponse.setHeader(HEADER_TOKEN, newToken);
        }

        return newToken;
    }

    private boolean doLogout(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws ServletException, IOException {
        String token = httpRequest.getHeader(HEADER_TOKEN);
        return authenticationService.logout(token);
    }

    private String currentLink(HttpServletRequest httpRequest) {
        if (httpRequest.getPathInfo() == null) {
            return httpRequest.getServletPath();
        }
        return httpRequest.getServletPath() + httpRequest.getPathInfo();
    }
}
