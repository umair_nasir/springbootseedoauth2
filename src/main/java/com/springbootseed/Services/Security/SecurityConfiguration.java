package com.springbootseed.Services.Security;

import com.springbootseed.Services.Security.AuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;
import java.util.Collections;

import static com.springbootseed.Constants.Headers.*;

/**
 * Created by umair.nasir on 3/29/17.
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
@Order(6)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Value("${endpoints.cors.allowed-origins}")
    private String CorsAllowedOrigins;

    @Value("${endpoints.cors.max-age}")
    private long CorsMaxAge;

    @Autowired
    AuthenticationFilter authenticationFilter;

    @Autowired
    AuthenticationEntryPoint unauthorizedEntryPoint;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS).permitAll()
                .antMatchers(HttpMethod.POST, "/facebook/**").permitAll()
                .anyRequest().fullyAuthenticated()
                .and()
                .addFilterBefore(authenticationFilter, UsernamePasswordAuthenticationFilter.class);
        http.csrf().disable();
        http.httpBasic().authenticationEntryPoint(unauthorizedEntryPoint);
        http.addFilterBefore(openEntityManagerInViewFilter(), UsernamePasswordAuthenticationFilter.class);
        http.addFilterBefore(corsFilterBean().getFilter(), UsernamePasswordAuthenticationFilter.class);
        http.logout().logoutRequestMatcher(req -> false);
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Bean
    public OpenEntityManagerInViewFilter openEntityManagerInViewFilter() {
        OpenEntityManagerInViewFilter bean = new OpenEntityManagerInViewFilter();
        return bean;
    }

    @Bean
    public FilterRegistrationBean corsFilterBean() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(false);
        config.setMaxAge(CorsMaxAge);
        config.addAllowedOrigin(CorsAllowedOrigins);
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        config.setAllowedMethods(Arrays.asList("GET", "PUT", "DELETE", "POST", "OPTIONS"));
        config.setAllowedHeaders(Arrays.asList(HEADER_TOKEN, HEADER_REQUEST_CLIENT, HEADER_CONTENT_TYPE, HEADER_ALLOW_HEADERS));
        config.setExposedHeaders(Collections.singletonList(HEADER_TOKEN));
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
        bean.setOrder(0);
        return bean;
    }

}
