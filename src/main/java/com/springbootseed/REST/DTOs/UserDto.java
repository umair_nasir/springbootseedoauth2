package com.springbootseed.REST.DTOs;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by umair.nasir on 3/23/17.
 */

@AllArgsConstructor(suppressConstructorProperties = true)
@NoArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto {

    String userName;
    int permissions;

}
