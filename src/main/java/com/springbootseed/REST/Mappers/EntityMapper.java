package com.springbootseed.REST.Mappers;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;

/**
 * Created by umair.nasir on 12/6/16.
 */
public abstract class EntityMapper<TEntity, TDto> {
    abstract public TDto toDto(TEntity entity);
    abstract public void updateFromDto(TEntity entity, TDto dto);
    abstract public TEntity createNewEntity();

    @Autowired
    private EntityManager entityManager;

    public Session getSession() {
        return entityManager.unwrap(Session.class);
    }
}