package com.springbootseed.REST.Endpoints;

import com.springbootseed.Persistence.Repositories.UserRepo;
import com.springbootseed.REST.DTOs.UserDto;
import com.springbootseed.REST.Mappers.UserMapper;
import com.springbootseed.Services.Security.ApiPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by umair.nasir on 3/23/17.
 */
@RestController
@RequestMapping("/user")
@Transactional
public class UserEndpoint {

    @Autowired
    UserRepo userRepo;

    @Autowired
    UserMapper userMapper;

    @PreAuthorize(ApiPermissions.ADMINISTER_ORGANIZATION_ANNOTATION)
    @RequestMapping(method = RequestMethod.GET)
    public List<UserDto> getAll() {
        return userRepo.findAll().stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }
}
