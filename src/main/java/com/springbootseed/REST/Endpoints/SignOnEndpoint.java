package com.springbootseed.REST.Endpoints;

import com.springbootseed.Persistence.Entities.Login;
import com.springbootseed.Persistence.Repositories.LoginRepo;
import com.springbootseed.Persistence.Repositories.OauthSessionRepo;
import com.springbootseed.REST.DTOs.AuthResponseDto;
import com.springbootseed.Services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by umair.nasir on 3/29/17.
 */

@RestController
public class SignOnEndpoint {

    @Autowired
    LoginRepo loginRepo;

    @Autowired
    AuthenticationService authenticationService;

    @Autowired
    OauthSessionRepo oauthSessionRepo;

    @RequestMapping(value = "/facebook/signOn", method = RequestMethod.POST)
    public String signOnFacebook(@RequestBody AuthResponseDto authResponseDto) {
        Login login = loginRepo.findByUserNameAndEmail(authResponseDto.getName(), authResponseDto.getEmail());
        Login facebookLogin = authenticationService.getFacebookMappedUser(authResponseDto.getAccessToken());
        if (login.equals(facebookLogin)) {
            String newOAuthSessionId = authenticationService.createNewOauth(authResponseDto.getAccessToken(), authResponseDto.getExpiresIn(), login);
            return authenticationService.createToken(login, newOAuthSessionId);
        }
        return null;
    }
}
