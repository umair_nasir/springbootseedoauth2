package com.springbootseed;

import com.springbootseed.Services.AuthenticationService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;

import static org.assertj.core.api.Java6Assertions.assertThat;

/**
 * Created by umair.nasir on 3/24/17.
 */

public class AuthenticationTests extends TestBase {

    @Autowired
    private AuthenticationService authenticationService;

    @Test(expected=AccessDeniedException.class)
    public void testGetFacebookUser_InvalidToken(){
        authenticationService.getFacebookMappedUser("123");
    }

    @Test
    public void testFakeToken() {
        assertThat(authenticationService.checkToken("fakeToken")).isNull();
    }
}
