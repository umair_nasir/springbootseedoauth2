package com.springbootseed;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by umair.nasir on 4/3/17.
 */

@ContextConfiguration
@WebAppConfiguration
public class UserEndpointTests extends TestBase {

    private MockMvc mvc;

    @Autowired
    private WebApplicationContext context;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void getUser_NoRoles() throws Exception {
        this.mvc.perform(get("/user")).andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser(roles=TestPermissions.USER_ORGANIZATION)
    public void getUser_Role_User() throws Exception {
        this.mvc.perform(get("/user")).andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser(roles=TestPermissions.ADMINISTER_ORGANIZATION)
    public void getUser_Role_Admin() throws Exception {
        this.mvc.perform(get("/user")).andExpect(status().isOk());
    }

    @Test
    public void getUser_Role_Admin_Specific() throws Exception {
        this.mvc.perform(get("/user").with(user("user").roles(TestPermissions.ADMINISTER_ORGANIZATION))).andExpect(status().isOk());
    }
}
